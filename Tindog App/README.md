### SWIFT COURSE 

### Install Dependencies with Cocoapods
CocoaPods is a dependencies bundler, as NPM, Bundler o Composer:
<pre>
sudo gem install cocoapods

OR

sudo gem uninstall cocoapods
gem install cocoapods
</pre>

Create Pods File:
<pre>
pod init
</pre>

Now, we have the Podfile where we will insert a new dependencie called **[RevealingSplashView](https://github.com/PiXeL16/RevealingSplashView)**:
<pre>
nano Podfile

target 'Tindog App' do
  # Comment the next line if you're not using Swift and don't want to use dynamic frameworks
  use_frameworks!

  <b>pod 'RevealingSplashView'</b>
  
  # Pods for Tindog App
end
</pre>

And now for install this library, we need to run the next command:
<pre>
pod install

## SUCCESS MESSAGE
[!] Automatically assigning platform `ios` with version `11.4` on target `Tindog App` because no platform was specified. Please specify a platform for this target in your Podfile. See `https://guides.cocoapods.org/syntax/podfile.html#platform`.
</pre>

### Export Images and Icon
When we need to handle image to our app, we need to download in the following sizes:
<pre>
  *.png
  *@2x.png
  *@3x.png
</pre>

The universal size to our app icon will be **240px**x**240px**. We can generate the whole sizes with on the website **[AppIconMaker](http://appiconmaker.co/)**.









